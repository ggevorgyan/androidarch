package com.github.arch.domain;

import android.annotation.SuppressLint;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.schedulers.Schedulers;

public class LoadReposUseCase {

    private static final String TAG = "LoadReposUseCase";

    @NonNull
    private final RemoteRepository mRemoteRepository;
    @NonNull
    private final LocalRepository mLocalRepository;

    public LoadReposUseCase(@NonNull RemoteRepository remoteRepository,
                            @NonNull LocalRepository localRepository) {
        mRemoteRepository = remoteRepository;
        mLocalRepository = localRepository;
    }

    @NonNull
    public Flowable<List<Repo>> getRepos() {
        // We load data from local repository, give it to our caller and then load data from remote repository.
        // When data from remote repository is loaded, we store it in out local repository and our caller automatically
        // gets notified about changed data.
        Flowable<List<Repo>> data = mLocalRepository.getReposList();
        updateRepos();
        return data;
    }

    @SuppressLint("CheckResult")
    public void updateRepos() {
        mRemoteRepository.getReposList().
                subscribeOn(Schedulers.io()).
                subscribe(
                        response -> {
                            if (response.isSuccessful()) {
                                List<Repo> list = response.body();
                                if (list != null) {
                                    mLocalRepository.insertOrUpdate(list);
                                }
                            }
                        },
                        throwable -> Log.e(TAG, "updateRepos()", throwable));
    }
}
