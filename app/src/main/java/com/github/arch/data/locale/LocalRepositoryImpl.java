package com.github.arch.data.locale;

import android.support.annotation.NonNull;

import com.github.arch.domain.LocalRepository;
import com.github.arch.domain.Repo;

import java.util.List;

import io.reactivex.Flowable;

public class LocalRepositoryImpl implements LocalRepository {

    @NonNull
    private final ReposDao mReposDao;

    public LocalRepositoryImpl(@NonNull ReposDao reposDao) {
        mReposDao = reposDao;
    }

    @NonNull
    @Override
    public Flowable<List<Repo>> getReposList() {
        return mReposDao.getReposList();
    }

    @Override
    public void insertOrUpdate(@NonNull List<Repo> list) {
        mReposDao.insertOrUpdate(list);
    }
}
