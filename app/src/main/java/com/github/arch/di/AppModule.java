package com.github.arch.di;

import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.annotation.NonNull;

import com.github.arch.data.locale.LocalRepositoryImpl;
import com.github.arch.data.locale.ReposDao;
import com.github.arch.data.locale.ReposDatabase;
import com.github.arch.data.remote.GithubService;
import com.github.arch.data.remote.RemoteRepositoryImpl;
import com.github.arch.domain.LoadReposUseCase;
import com.github.arch.domain.LocalRepository;
import com.github.arch.domain.RemoteRepository;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

@Module
public class AppModule {

    @NonNull
    private Context mContext;

    public AppModule(@NonNull Context appContext) {
        mContext = appContext;
    }

    @Provides
    @Singleton
    GithubService provideGithubService() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BASIC);
        OkHttpClient client = new OkHttpClient.Builder().
                addInterceptor(interceptor).
                build();

        return new Retrofit.Builder()
                .baseUrl(GithubService.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(GithubService.class);
    }

    @Provides
    @Singleton
    ReposDatabase provideReposDatabase() {
        return Room.databaseBuilder(
                mContext,
                ReposDatabase.class,
                ReposDatabase.DATABASE_NAME).
                build();
    }

    @Provides
    @Singleton
    ReposDao provideReposDao(@NonNull ReposDatabase reposDatabase) {
        return reposDatabase.reposDao();
    }

    @Provides
    @Singleton
    LoadReposUseCase provideLoadReposUseCase(@NonNull LocalRepository localRepository,
                                             @NonNull RemoteRepository remoteRepository) {
        return new LoadReposUseCase(remoteRepository, localRepository);
    }

    @Provides
    @Singleton
    LocalRepository provideLocalRepository(@NonNull ReposDao reposDao) {
        return new LocalRepositoryImpl(reposDao);
    }

    @Provides
    @Singleton
    RemoteRepository provideRemoteRepository(@NonNull GithubService githubService) {
        return new RemoteRepositoryImpl(githubService);
    }
}